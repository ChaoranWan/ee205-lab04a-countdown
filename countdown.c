///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author TODO_name <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>


int main(int argc, char* argv[]) {
   time_t chosen, now;
   long diff;
   struct tm * timenow;
   struct tm timechosen = {0};

   int years, days, minutes, hours, extra;

   timechosen.tm_year = 2001 - 1900;
   timechosen.tm_mon = 2 - 1;
   timechosen.tm_mday = 2;
   timechosen.tm_hour = 1;
   timechosen.tm_min = 1;
   timechosen.tm_sec = 1;

   chosen = mktime(&timechosen);
   printf("Reference Time: %s\n",ctime(&chosen));

   time (&now);
   timenow = localtime ( &now );
   //printf ( "Current time: %d\n", now);
      //printf ( "Current local time and date: %s", asctime (timenow));
      //printf ( "Chosen time: %d\n", chosen);
   diff = difftime(now, chosen);

   while(1) {
      diff = diff + 1;
      years = diff / 31536000;
      printf ("Years: %d ",years);
      extra = diff % 31536000;
      days = extra / 86400;
      printf ("Days: %d ",days);
      extra = extra % 86400;
      hours = extra / 3600;
      printf ("Hours: %d ",hours);
      extra = extra % 3600;
      minutes = extra / 60;
      printf ("Minutes: %d ",minutes);
      extra = extra % 60;
      printf ("Seconds: %d\n", extra);
      sleep(1);

      //printf("%d\n",diff);

   }

   return 0;
}
